<?php
	class Conexion
	{
		public function connec()  
		{
			$servername = "127.0.0.1";
			$database = "alpha";
			$username = "hbody";
			$password = "qwerty";
			// Create connection
			$conn = mysqli_connect($servername, $username, $password, $database);
			mysqli_query($conn,"SET NAMES 'UTF8'");
			// Check connection
			if (!$conn) {
				echo("Error: aqui" . mysqli_connect_error());
			}
			return $conn;
		}
		public function crud($val, $sql,$string,$params) {
			$bd = new Conexion();
            $conn = $bd->connec();
            
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				if ($params != null){
					mysqli_stmt_bind_param($result, $string, $params);
				}
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if(mysqli_errno($conn) == 0){
					if ($val == 'SELECT') {
						while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
							
							array_push($ingre, $row);
						}
						return array(1,$ingre);
					}
					else if ($val == 'UPDATE') {
						return array(1,"Acualizado con exito");
					}
					else if ($val == 'INSERT') {
						return array(1,"Ingresado con exito");
					}
				}
				else{
					$mensaje = 'Algo salio mal: '.mysqli_errno($conn);
					switch(mysqli_errno($conn)){
						case 1045:
							$mensaje = 'Autenticacion desconocida';
						break;
						case 1049:
							$mensaje = 'Base desconocida';
						break;
						case 1054:
							$mensaje = 'Nombre del campo desconocido';
						break;
						case 1062:
							$mensaje = 'datos duplicados no se puede guardar';
						break;
						case 1146:
							$mensaje = 'Nombre de la tabla desconocido';
						break;
						case 1451:
							$mensaje = 'Registro ocupado no se puede eliminar';
						break;
						case 2002:
							$mensaje = 'Servidor desconocido';
						break;
					}
					return array(0,$mensaje);
				}
			}
			else{
				$mensaje = 'Algo salio mal';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				return array(0,$mensaje);
			}
		}
		public function crud2($val,$sql,$string,$p1,$p2) {
			$bd = new Conexion();
            $conn = $bd->connec();
            
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result,$string, $p1,$p2);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if(mysqli_errno($conn) == 0){
					if ($val == 'SELECT') {
						while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
							
							array_push($ingre, $row);
						}
						return array(1,$ingre);
					}
					else if ($val == 'UPDATE') {
						return array(1,"Acualizado con exito");
					}
					else if ($val == 'INSERT') {
						return array(1,"Ingresado con exito");
					}
				}
				else{
					$mensaje = 'Algo salio mal: '.mysqli_errno($conn);
					switch(mysqli_errno($conn)){
						case 1045:
							$mensaje = 'Autenticacion desconocida';
						break;
						case 1049:
							$mensaje = 'Base desconocida';
						break;
						case 1054:
							$mensaje = 'Nombre del campo desconocido';
						break;
						case 1062:
							$mensaje = 'datos duplicados no se puede guardar';
						break;
						case 1146:
							$mensaje = 'Nombre de la tabla desconocido';
						break;
						case 1451:
							$mensaje = 'Registro ocupado no se puede eliminar';
						break;
						case 2002:
							$mensaje = 'Servidor desconocido';
						break;
					}
					return array(0,$mensaje);
				}
			}
			else{
				$mensaje = 'Algo salio muy mal: ';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				return array(0,$mensaje);
			}
		}
		public function crud3($val,$sql,$string,$p1,$p2,$p3) {
			$bd = new Conexion();
            $conn = $bd->connec();
            
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result,$string, $p1,$p2,$p3);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if(mysqli_errno($conn) == 0){
					if ($val == 'SELECT') {
						while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
							
							array_push($ingre, $row);
						}
						return array(1,$ingre);
					}
					else if ($val == 'UPDATE') {
						return array(1,"Acualizado con exito");
					}
					else if ($val == 'INSERT') {
						return array(1,"Ingresado con exito");
					}
				}
				else{
					$mensaje = 'Algo salio mal: '.mysqli_errno($conn);
					switch(mysqli_errno($conn)){
						case 1045:
							$mensaje = 'Autenticacion desconocida';
						break;
						case 1049:
							$mensaje = 'Base desconocida';
						break;
						case 1054:
							$mensaje = 'Nombre del campo desconocido';
						break;
						case 1062:
							$mensaje = 'datos duplicados no se puede guardar';
						break;
						case 1146:
							$mensaje = 'Nombre de la tabla desconocido';
						break;
						case 1451:
							$mensaje = 'Registro ocupado no se puede eliminar';
						break;
						case 2002:
							$mensaje = 'Servidor desconocido';
						break;
					}
					return array(0,$mensaje);
				}
			}
			else{
				$mensaje = 'Algo salio muy mal: ';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				return array(0,$mensaje);
			}
		}
		public function crud6($val,$sql,$string,$p1,$p2,$p3,$p4,$p5,$p6) {
			$bd = new Conexion();
            $conn = $bd->connec();
            
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result,$string, $p1,$p2,$p3,$p4,$p5,$p6);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if(mysqli_errno($conn) == 0){
					if ($val == 'SELECT') {
						while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
							
							array_push($ingre, $row);
						}
						return array(1,$ingre);
					}
					else if ($val == 'UPDATE') {
						return array(1,"Acualizado con exito");
					}
					else if ($val == 'INSERT') {
						return array(1,"Ingresado con exito");
					}
				}
				else{
					$mensaje = 'Algo salio mal: '.mysqli_errno($conn);
					switch(mysqli_errno($conn)){
						case 1045:
							$mensaje = 'Autenticacion desconocida';
						break;
						case 1049:
							$mensaje = 'Base desconocida';
						break;
						case 1054:
							$mensaje = 'Nombre del campo desconocido';
						break;
						case 1062:
							$mensaje = 'datos duplicados no se puede guardar';
						break;
						case 1146:
							$mensaje = 'Nombre de la tabla desconocido';
						break;
						case 1451:
							$mensaje = 'Registro ocupado no se puede eliminar';
						break;
						case 2002:
							$mensaje = 'Servidor desconocido';
						break;
					}
					return array(0,$mensaje);
				}
			}
			else{
				$mensaje = 'Algo salio mal';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				return array(0,$mensaje);
			}
		}
		public function crud8($val,$sql,$string,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
			$bd = new Conexion();
            $conn = $bd->connec();
            
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result,$string, $p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8);
				mysqli_stmt_execute($result);
				$row = array();
				if(mysqli_errno($conn) == 0){
					if ($val == 'SELECT') {
						while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
							
							array_push($ingre, $row);
						}
						return array(1,$ingre);
					}
					else if ($val == 'UPDATE') {
						return array(1,"Acualizado con exito");
					}
					else if ($val == 'INSERT') {
						return array(1,"Ingresado con exito");
					}
				}
				else{
					$mensaje = 'Algo salio mal: '.mysqli_errno($conn);
					switch(mysqli_errno($conn)){
						case 1045:
							$mensaje = 'Autenticacion desconocida';
						break;
						case 1049:
							$mensaje = 'Base desconocida';
						break;
						case 1054:
							$mensaje = 'Nombre del campo desconocido';
						break;
						case 1062:
							$mensaje = 'datos duplicados no se puede guardar';
						break;
						case 1146:
							$mensaje = 'Nombre de la tabla desconocido';
						break;
						case 1451:
							$mensaje = 'Registro ocupado no se puede eliminar';
						break;
						case 2002:
							$mensaje = 'Servidor desconocido';
						break;
					}
					return array(0,$mensaje);
				}
			}
			else{
				$mensaje = 'Algo salio mal';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				return array(0,$mensaje);
			}
		}
		public function crud9($val,$sql,$string,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9) {
			$bd = new Conexion();
            $conn = $bd->connec();
            
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result,$string, $p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if(mysqli_errno($conn) == 0){
					if ($val == 'SELECT') {
						while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
							
							array_push($ingre, $row);
						}
						return array(1,$ingre);
					}
					else if ($val == 'UPDATE') {
						return array(1,"Acualizado con exito");
					}
					else if ($val == 'INSERT') {
						return array(1,"Ingresado con exito");
					}
				}
				else{
					$mensaje = 'Algo salio mal: '.mysqli_errno($conn);
					switch(mysqli_errno($conn)){
						case 1045:
							$mensaje = 'Autenticacion desconocida';
						break;
						case 1049:
							$mensaje = 'Base desconocida';
						break;
						case 1054:
							$mensaje = 'Nombre del campo desconocido';
						break;
						case 1062:
							$mensaje = 'datos duplicados no se puede guardar';
						break;
						case 1146:
							$mensaje = 'Nombre de la tabla desconocido';
						break;
						case 1451:
							$mensaje = 'Registro ocupado no se puede eliminar';
						break;
						case 2002:
							$mensaje = 'Servidor desconocido';
						break;
					}
					return array(0,$mensaje);
				}
			}
			else{
				$mensaje = 'Algo salio mal';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				return array(0,$mensaje);
			}
		}
	}
?>

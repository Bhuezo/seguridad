<?php 
    class Validacion
    {
        public function correos($email="") {
            if (preg_match("/^([a-zA-Z0-9&])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-])*.([a-zA-Z])+$/", $email)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function fechas($fecha="") {
            if (preg_match("/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})+$/", $fecha)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function letras($letra="") {
            if (preg_match("/^([a-zA-Z- &]{3,100})+$/", $letra)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function telefonos($telefono="") {
            if (preg_match("/^([0-9]{8})+$/", $telefono)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function precios($precios="") {
            if (preg_match("/^([0-9])*.([0-9]{0,2})+$/", $precios)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function numeros($numero="") {
            if (preg_match("/^([0-9])+$/", $numero)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function contra($c,$r) {
            if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/", $c)) {
                if ($c == $r) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        
        public function privilegios($privilegio="") {
            if (preg_match("/^([0-1]{10})+$/", $privilegio)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function combobox($box="") {
            if ($box != "Elegir...") {
                return true;
            }
            else {
                return false;
            }
        }
        public function generos($letra="") {
            if ($letra == 'Masculino') {
                return true;
            }
            else if ($letra == 'Femenino') {
                return true;
            }
            else {
                return false;
            }
        }
        public function dui($numero="") {
            if (preg_match("/^([0-9]{9})+$/", $numero)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function nit($numero="") {
            if (preg_match("/^([0-9]{14})+$/", $numero)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function letrasNumeros($numero="") {
            if (preg_match("/^([a-zA-Z0-9])*$/", $numero)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function validateTime($horas="") {
         if(preg_match("/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])[\:]([0-5][0-9])$/",$horas)) {
                return true;
            }
            else {
               return false;
            }     
        }
        public function validateTime1($horas="") {
         if(preg_match("/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])$/",$horas)) {
                return true;
            }
            else {
               return false;
            }     
        }
    }
?>
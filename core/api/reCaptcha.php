<?php
error_reporting(E_ALL ^ E_NOTICE);  //variables not declared warning off

// tu clave secreta
$secret = "6LcbrrQUAAAAAMOaSbUQmMe2UHmCLQwn9Bnt5aQI";   //get from https://www.google.com/recaptcha/
// tu site key
$sitekey = "6LcbrrQUAAAAALtcPkA92VqK2bVa98loPms2s2BI";  //get from https://www.google.com/recaptcha/


require_once "../lib/recaptchalib.php";

$response = null; 
$reCaptcha = new ReCaptcha($secret);


if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
    if (isset($_POST["response"])) {
            $response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["response"]
        );
    }
    //if ($response != null && $response->success) {
    if ($response->success) {
        $res['status'] = 1;
        $res['message'] = 'Captcha valido';
    }
    else{
        $res['status'] = 0;
        $res['message'] = 'Captcha no valido';
    }

    header( 'Content-type: application/json');
    echo json_encode($res);
}


?>
<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    include('../models/CRUD_empleados.php');
    include('../helpers/validaciones.php');
    ini_set('date.timezone', 'America/El_Salvador');
    session_start();
    $crud =  new Empleados();
    $val = new Validacion();

    #Variable que verificara el privilegio osea si es para dashboard o para public la consulta
    $privilege = '';
    #variable que contiene la accion que se solicita
    $action = '';

    #Verificar que si se a mandado el privilegio
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }
    #Varfiicar que se a mandado la acción a realizar
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    $res = null;
    #verificar el privilegio
    switch ($privilege) {
        case 'private':
            #Acciones que se pueden realizar ya iniciando sesion
            switch ($action) {
                #Apis para con sesion
                case 'create':
                    if (isset($_SESSION['id_empleado'])) {
                        if ( isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['correo']) && isset($_POST['alias'])
                        && isset($_POST['fecha']) && isset($_POST['intentos']) && isset($_POST['id_tipo'])  
                        && !empty($_POST['contra']) && !empty($_POST['repetir'])) {
                            #Verificar Alias con solo letras y numeros
                            if ($val->letrasNumeros($_POST['alias'])) {
                                #Verificar Nombre con solo letras
                                if ($val->letras($_POST['nombre'])) {
                                    #Verificar Apellido con solo letras
                                    if ($val->letras($_POST['apellido'])) {
                                        #verificar Correo
                                        if ($val->correos($_POST['correo'])) {
                                            #Verificar Fecha de Nacimiento
                                            if ($val->fechas($_POST['fecha'])) {
                                                #Verificar intentos de Nacimiento
                                                if ($val->numeros($_POST['intentos'])) {
                                                    #Validar similitud entre contraseña y confirmar contraseña
                                                    if ($val->contra($_POST['contra'], $_POST['repetir'])) {
                                                        $n = strtolower($_POST['nombre']);
                                                        $a = strtolower($_POST['apellido']);
                                                        $r = strtolower($_POST['contra']);
                                                        #Validar que nombre y/o apellido este contenido en la contraseña
                                                        if ($val->numeros(strrpos($r, $n)) == false && $val->numeros(strrpos($r, $a)) == false) {
                                                            #Ingresamos nuevo usuario
                                                            $resultado = $crud->create(array(
                                                                $_POST['alias'],
                                                                $_POST['nombre'],
                                                                $_POST['apellido'],
                                                                $_POST['correo'],
                                                                password_hash($_POST['contra'],PASSWORD_DEFAULT),
                                                                $_POST['fecha'],
                                                                $_POST['id_tipo'],
                                                                $_POST['intentos'],
                                                            ));
                                                            #validar si todo esta correcto
                                                            if ($resultado[0] == 1) {
                                                                $res['dataset'] = $resultado[1];
                                                                $res['status'] = 1;
                                                            } elseif ($resultado[0] == 0) {
                                                                $res['message'] = $resultado[1];
                                                                $res['status'] = 0;
                                                            }
                                                        } else {
                                                            $res['message'] = 'Se encontro similitudes en la contraseña y el nombre';
                                                            $res['status'] = 0;
                                                        }
                                                    } else {
                                                        $res['message'] = 'Contraseñas no validas';
                                                        $res['status'] = 0;
                                                    }
                                                } else {
                                                    $res['message'] = 'Valor de intentos no valido';
                                                    $res['status'] = 0;
                                                }
                                            } else {
                                                $res['message'] = 'Fecha no valido';
                                                $res['status'] = 0;
                                            }
                                        } else {
                                            $res['message'] = 'Correo no valido';
                                            $res['status'] = 0;
                                        }
                                    } else {
                                        $res['message'] = 'Apellido no valido';
                                        $res['status'] = 0;
                                    }
                                } else {
                                    $res['message'] = 'Nombre no valido';
                                    $res['status'] = 0;
                                }
                            } else {
                                $res['message'] = 'Alias no valido';
                                $res['status'] = 0;
                            }
                        } else {
                            $res['message'] = 'Valores vacíos';
                            $res['status'] = 0;
                        }
                    }
                    else {
                        $res['message'] = 'No hay sesion';
                        $res['status'] = 0;
                    }
                break;
                case 'read':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->readEmpleado();
                        if ($resultado[0] == 1) {
                            if (count($resultado[1]) > 0) {
                                $res['status'] = 1;
                                $res['dataset'] = $resultado[1];
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No hay usuarios';
                            }
                        }
                        else if ($resultado[0] == 0) {
                            $res['status'] = 0;
                            $res['message'] = $resultado[1];
                        }
                    }
                    else {
                        $res['message'] = 'No hay sesion';
                        $res['status'] = 0;
                    }
                break;
                case 'update':
                    if (isset($_SESSION['id_empleado'])) {
                        if ( isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['correo']) && isset($_POST['alias'])
                        && isset($_POST['fecha']) && isset($_POST['intentos']) && isset($_POST['id_tipo']) && isset($_POST['id']) 
                        && !empty($_POST['contra']) && !empty($_POST['repetir'])) {
                            #Verificar Alias con solo letras y numeros
                            if ($val->letrasNumeros($_POST['alias'])) {
                                #Verificar Nombre con solo letras
                                if ($val->letras($_POST['nombre'])) {
                                    #Verificar Apellido con solo letras
                                    if ($val->letras($_POST['apellido'])) {
                                        #verificar Correo
                                        if ($val->correos($_POST['correo'])) {
                                            #Verificar Fecha de Nacimiento
                                            if ($val->fechas($_POST['fecha'])) {
                                                #Verificar intentos de Nacimiento
                                                if ($val->numeros($_POST['intentos'])) {
                                                    #Validar similitud entre contraseña y confirmar contraseña
                                                    if ($val->contra($_POST['contra'], $_POST['repetir'])) {
                                                        $n = strtolower($_POST['nombre']);
                                                        $a = strtolower($_POST['apellido']);
                                                        $r = strtolower($_POST['contra']);
                                                        #Validar que nombre y/o apellido este contenido en la contraseña
                                                        if ($val->numeros(strrpos($r, $n)) == false && $val->numeros(strrpos($r, $a)) == false) {
                                                            $resultado = $crud->contraVerify(array(
                                                                $_POST['id']
                                                            ));
                                                            if ($resultado[0] == 1) {
                                                                if (count($resultado[1]) > 0) {
                                                                    if ( password_verify($_POST['contra'], $resultado[1][0]['contra']) ) {
                                                                        $res['status'] = 0;
                                                                        $res['message'] = 'La contraseña es igual a la anterior';
                                                                    }
                                                                    else {#Ingresar primer usuario
                                                                        $resultado = $crud->update(array(
                                                                            $_POST['alias'],
                                                                            $_POST['nombre'],
                                                                            $_POST['apellido'],
                                                                            $_POST['correo'],
                                                                            $_POST['fecha'],
                                                                            $_POST['id_tipo'],
                                                                            $_POST['intentos'],
                                                                            password_hash($_POST['contra'],PASSWORD_DEFAULT),
                                                                            $_POST['id']
                                                                        ));
                                                                        #validar si todo esta correcto
                                                                        if ($resultado[0] == 1) {
                                                                            $res['dataset'] = $resultado[1];
                                                                            $res['status'] = 1;
                                                                        } elseif ($resultado[0] == 0) {
                                                                            $res['message'] = $resultado[1];
                                                                            $res['status'] = 0;
                                                                        }
                                                                    }
                                                                }
                                                                else {
                                                                    $res['status'] = 0;
                                                                    $res['message'] = 'Su usuario dejo de existir';
                                                                }
                                                            }
                                                            else if ($resultado[0] == 0) {
                                                                $res['status'] = 0;
                                                                $res['message'] = $resultado[1];
                                                            }
                                                        } else {
                                                            $res['message'] = 'Se encontro similitudes en la contraseña y el nombre';
                                                            $res['status'] = 0;
                                                        }
                                                    } else {
                                                        $res['message'] = 'Contraseñas no validas';
                                                        $res['status'] = 0;
                                                    }
                                                } else {
                                                    $res['message'] = 'Valor de intentos no valido';
                                                    $res['status'] = 0;
                                                }
                                            } else {
                                                $res['message'] = 'Fecha no valido';
                                                $res['status'] = 0;
                                            }
                                        } else {
                                            $res['message'] = 'Correo no valido';
                                            $res['status'] = 0;
                                        }
                                    } else {
                                        $res['message'] = 'Apellido no valido';
                                        $res['status'] = 0;
                                    }
                                } else {
                                    $res['message'] = 'Nombre no valido';
                                    $res['status'] = 0;
                                }
                            } else {
                                $res['message'] = 'Alias no valido';
                                $res['status'] = 0;
                            }
                        }
                        else if ( isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['correo']) && isset($_POST['alias'])
                        && isset($_POST['fecha']) && isset($_POST['intentos']) && isset($_POST['id_tipo']) && isset($_POST['id'])) {
                            #Verificar Alias con solo letras y numeros
                            if ($val->letrasNumeros($_POST['alias'])) {
                                #Verificar Nombre con solo letras
                                if ($val->letras($_POST['nombre'])) {
                                    #Verificar Apellido con solo letras
                                    if ($val->letras($_POST['apellido'])) {
                                        #verificar Correo
                                        if ($val->correos($_POST['correo'])) {
                                            #Verificar Fecha de Nacimiento
                                            if ($val->fechas($_POST['fecha'])) {
                                                #Verificar intentos de Nacimiento
                                                if ($val->numeros($_POST['intentos'])) {
                                                    #Ingresar primer usuario
                                                    $resultado = $crud->updateSP(array(
                                                        $_POST['alias'],
                                                        $_POST['nombre'],
                                                        $_POST['apellido'],
                                                        $_POST['correo'],
                                                        $_POST['fecha'],
                                                        $_POST['id_tipo'],
                                                        $_POST['intentos'],
                                                        $_POST['id']
                                                    ));
                                                    #validar si todo esta correcto
                                                    if ($resultado[0] == 1) {
                                                        $res['dataset'] = $resultado[1];
                                                        $res['status'] = 1;
                                                    } elseif ($resultado[0] == 0) {
                                                        $res['message'] = $resultado[1];
                                                        $res['status'] = 0;
                                                    }
                                                } else {
                                                    $res['message'] = 'Valor de intentos no valido';
                                                    $res['status'] = 0;
                                                }
                                            } else {
                                                $res['message'] = 'Fecha no valido';
                                                $res['status'] = 0;
                                            }
                                        } else {
                                            $res['message'] = 'Correo no valido';
                                            $res['status'] = 0;
                                        }
                                    } else {
                                        $res['message'] = 'Apellido no valido';
                                        $res['status'] = 0;
                                    }
                                } else {
                                    $res['message'] = 'Nombre no valido';
                                    $res['status'] = 0;
                                }
                            } else {
                                $res['message'] = 'Alias no valido';
                                $res['status'] = 0;
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Valores vacios';
                        }
                    }
                    else {
                        $res['message'] = 'No hay sesion';
                        $res['status'] = 0;
                    }
                break;
                case 'readPersonal':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->readPersonal(array(
                            $_SESSION['id_empleado']
                        ));
                        if ($resultado[0] == 1) {
                            if (count($resultado[1]) > 0) {
                                if ($resultado[1][0]['atributos'] != '0000') {
                                    if ($resultado[1][0]['intentos'] < 3) {
                                        $res['status'] = 1;
                                        $res['dataset'] = $resultado[1];
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Su usuario ha sido bloqueado';
                                    }
                                }
                                else {
                                    $_SESSION['id_empleado'] = null;
                                    $res['status'] = 0;
                                    $res['message'] = 'Su usuario ha sido removido de atributos';
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Su usuario dejo de existir';
                            }
                        }
                        else if ($resultado[0] == 0) {
                            $res['status'] = 0;
                            $res['message'] = $resultado[1];
                        }
                    }
                    else {
                        $res['status'] = 0;
                    }
                break;
                case 'updatePersonal':
                #verificar que los Post fueron mandados correctamente
                    if (isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['correo'])
                     && isset($_POST['repetir']) && isset($_POST['fecha'])
                    && isset($_POST['alias'])) {
                        #Verificar Alias con solo letras y numeros
                        if ($val->letrasNumeros($_POST['alias'])) {
                            #Verificar Nombre con solo letras
                            if ($val->letras($_POST['nombre'])) {
                                #Verificar Apellido con solo letras
                                if ($val->letras($_POST['apellido'])) {
                                    #verificar Correo
                                    if ($val->correos($_POST['correo'])) {
                                        #Verificar Fecha de Nacimiento
                                        if ($val->fechas($_POST['fecha'])) {
                                            #Ingresar primer usuario
                                            $resultado = $crud->updatePersonal(array(
                                                $_POST['alias'],
                                                $_POST['nombre'],
                                                $_POST['apellido'],
                                                $_POST['correo'],
                                                $_POST['fecha'],
                                                $_SESSION['id_empleado']
                                            ));
                                            #validar si todo esta correcto
                                            if ($resultado[0] == 1) {
                                                $res['dataset'] = $resultado[1];
                                                $res['status'] = 1;
                                            } elseif ($resultado[0] == 0) {
                                                $res['message'] = $resultado[1];
                                                $res['status'] = 0;
                                            }
                                        } else {
                                            $res['message'] = 'Fecha no valido';
                                            $res['status'] = 0;
                                        }
                                    } else {
                                        $res['message'] = 'Correo no valido';
                                        $res['status'] = 0;
                                    }
                                } else {
                                    $res['message'] = 'Apellido no valido';
                                    $res['status'] = 0;
                                }
                            } else {
                                $res['message'] = 'Nombre no valido';
                                $res['status'] = 0;
                            }
                        } else {
                            $res['message'] = 'Alias no valido';
                            $res['status'] = 0;
                        }
                    } else {
                        $res['message'] = 'Valores Vacíos';
                        $res['status'] = 0;
                    }
                break;
                #Apis solo para no sesion
                case 'sesion':
                    if (isset($_SESSION['id_empleado'])) {
                        $res['status'] = 1;
                    } else {
                        $res['status'] = 0;
                    }
                break;
                case 'valPrimer':
                    $resultado = $crud->valPrimero();
                    //$_SESSION['id_empleado'] = null;
                    if (count($resultado[1]) == 0) {
                        $res['status'] = 1;
                    } else {
                        $res['status'] = 0;
                    }
                break;
                case 'primer':
                #verificar que los Post fueron mandados correctamente
                    if (isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['correo'])
                    && isset($_POST['contra']) && isset($_POST['repetir']) && isset($_POST['fecha'])
                    && isset($_POST['alias'])) {
                        #Verificar Alias con solo letras y numeros
                        if ($val->letrasNumeros($_POST['alias'])) {
                            #Verificar Nombre con solo letras
                            if ($val->letras($_POST['nombre'])) {
                                #Verificar Apellido con solo letras
                                if ($val->letras($_POST['apellido'])) {
                                    #verificar Correo
                                    if ($val->correos($_POST['correo'])) {
                                        #Verificar Fecha de Nacimiento
                                        if ($val->fechas($_POST['fecha'])) {
                                            #Validar similitud entre contraseña y confirmar contraseña
                                            if ($val->contra($_POST['contra'], $_POST['repetir'])) {
                                                $n = strtolower($_POST['nombre']);
                                                $a = strtolower($_POST['apellido']);
                                                $p = strtolower($_POST['alias']);
                                                $r = strtolower($_POST['contra']);
                                                #Validar que nombre y/o apellido este contenido en la contraseña
                                                if ($val->numeros(strrpos($r, $n)) == false && $val->numeros(strrpos($r, $a)) == false && $val->numeros(strrpos($r, $p)) == false) {
                                                    #Validar primer privilegio, para crear uno sino existen
                                                    $resultado = $crud->primerPrivilege();
                                                    if ($resultado[0] == 1) {
                                                        #validar primer usuario por si es optimo crear uno o ya hay uno
                                                        $resultado = $crud->valPrimero();
                                                        if (count($resultado[1]) == 0) {
                                                            #Ingresar primer usuario
                                                            $resultado = $crud->primerUser(array(
                                                                $_POST['alias'],
                                                                $_POST['nombre'],
                                                                $_POST['apellido'],
                                                                $_POST['correo'],
                                                                $_POST['contra'],
                                                                $_POST['fecha']
                                                            ));
                                                            #validar si todo esta correcto
                                                            if ($resultado[0] == 1) {
                                                                $res['dataset'] = $resultado[1];
                                                                $res['status'] = 1;
                                                            } elseif ($resultado[0] == 0) {
                                                                $res['message'] = $resultado[1];
                                                                $res['status'] = 0;
                                                            }
                                                        } else {
                                                            $res['message'] = 'ya existe un primer usuario';
                                                            $res['status'] = 0;
                                                        }
                                                    } elseif ($resultado[0] == 0) {
                                                        $res['message'] = 'E1'.$resultado[1];
                                                        $res['status'] = 0;
                                                    }
                                                } else {
                                                    $res['message'] = 'Se encontro similitudes en la contraseña y el nombre';
                                                    $res['status'] = 0;
                                                }
                                            } else {
                                                $res['message'] = 'Contraseñas no validas';
                                                $res['status'] = 0;
                                            }
                                        } else {
                                            $res['message'] = 'Fecha no valido';
                                            $res['status'] = 0;
                                        }
                                    } else {
                                        $res['message'] = 'Correo no valido';
                                        $res['status'] = 0;
                                    }
                                } else {
                                    $res['message'] = 'Apellido no valido';
                                    $res['status'] = 0;
                                }
                            } else {
                                $res['message'] = 'Nombre no valido';
                                $res['status'] = 0;
                            }
                        } else {
                            $res['message'] = 'Alias no valido';
                            $res['status'] = 0;
                        }
                    } else {
                        $res['message'] = 'Valores Vacíos';
                        $res['status'] = 0;
                    }
                break;
                case 'login':
                #Verificar POST enviados
                    if (isset($_POST['contra']) && isset($_POST['alias'])) {
                        #validar compos vacios
                        if (!empty($_POST['contra']) && !empty($_POST['alias'])) {
                            #Verificar existencia de alias en la base
                            $resultado = $crud->login(array(
                                $_POST['alias'],
                            ));
                            #Fecha de ahora
                            $hoy = date("Y-m-d");
                            #Validar Consulta o algun contra tiempo en la consulta
                            if ($resultado[0] == 1) {
                                #Consultar si el alias existe y es mayor que 0 es que existe
                                if (count($resultado[1]) > 0) {
                                    if($resultado[1][0]['atributos'] != '0000'){
                                        if ($resultado[1][0]['fecha_estado'] == $hoy && $resultado[1][0]["estado"] == 1) {
                                            $vel = false;
                                        } else {
                                            $vel = true;
                                        }
                                        if ($vel) {
                                            #Comparar contraseñas
                                            if (password_verify($_POST['contra'], $resultado[1][0]["contra"])) {
                                                #Reiniciar contador
                                                $_SESSION['intentos'] = null;
                                                $_SESSION['id_empleado'] = $resultado[1][0]["id_usuario"];
                                                $res['status'] = 1;
                                                #Ingresar sesion para iniciar el dashboard
                                                $resultado = $crud->ingresar(array(
                                                        $_POST['alias']
                                                    ));
                                                if ($resultado[0] == 0) {
                                                    $res['message'] = $resultado[1];
                                                    $res['status'] = 0;
                                                }
                                            }
                                            #Error contraseña incorrecta
                                            else {
                                                $intento = $resultado[1][0]["intentos"];
                                                $res['message'] = 'Correo y Contraseña no coinciden: intentos '.($intento+1);
                                                $res['status'] = 0;
                                                #Ir validando intentos hasta llegar a 3
                                                $resultado = $crud->intentos(array(
                                                        ($intento+1),
                                                        $_POST['alias']
                                                    ));
                                                if ($resultado[0] == 0) {
                                                    $res['message'] = $resultado[1];
                                                    $res['status'] = 0;
                                                }
                                            }
                                        }
                                        #El Alias ya tiene una sesión
                                        else {
                                            $res['message'] = 'Ya existe una sesion';
                                            $res['status'] = 0;
                                        }
                                    }
                                    #El Alias ya tiene una sesión
                                    else {
                                        $res['message'] = 'Usuario Bloqueado';
                                        $res['status'] = 0;
                                    }
                                }
                                #El Alias no exite
                                elseif (count($resultado[1]) == 0) {
                                    $res['message'] = 'Alias no existe o esta bloqueado';
                                    $res['status'] = 0;
                                }
                            } elseif ($resultado[0] == 0) {
                                $res['status'] = 0;
                                $res['message'] = $resultado[1];
                            }
                        } else {
                            $res['message'] = 'Valores Vacíos';
                            $res['status'] = 0;
                        }
                    } else {
                        $res['message'] = 'Valores Vacíos';
                        $res['status'] = 0;
                    }
                break;
                case 'bloquear':
                    
                break;
                case 'requiredPassword':
                    if (isset($_POST['correo'])) {
                        if (!empty($_POST['correo'])) {
                            $resultado = $crud->valCorreo(array(
                                $_POST['correo']
                            ));
                            if ($resultado[0] == 1) {
                                if (count($resultado[1]) > 0) {
                                    $length = 10;
                                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                    $charactersLength = strlen($characters);
                                    $randomString = '';
                                    for ($i = 0; $i < $length; $i++) {
                                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                                    }
                                    $resultado = $crud->changePassword(array(
                                        password_hash($randomString, PASSWORD_DEFAULT),
                                        $_POST['correo']
                                    ));
                                    if ($resultado[0] == 1) {
                                        require '../lib/PHPMailer/Exception.php';
                                        require '../lib/PHPMailer/PHPMailer.php';
                                        require '../lib/PHPMailer/SMTP.php';
                                        // Instantiation and passing `true` enables exceptions
                                        $mail = new PHPMailer(true);
                
                                        try {
                                            //Server settings
                                            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
                                            $mail->isSMTP();                                            // Set mailer to use SMTP
                                            $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                                            $mail->Username   = 'api.local777@gmail.com';                     // SMTP username
                                            $mail->Password   = 'Qwerty123$';                               // SMTP password
                                            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                                            $mail->Port       = 587;                                    // TCP port to connect to
            
                                            //Recipients
                                            $mail->setFrom('api.local777@gmail.com', 'moedicion');
                                            $mail->addAddress($_POST['correo']);
                
                
                                            // Content
                                                $mail->isHTML(true);                                  // Set email format to HTML
                                                $mail->Subject = 'Recuperar Contraseña';
                                            $mail->Body    = "
                                                    <h1>Se ha colocado como contraseña la siguiente:</h1>
                                                    <h3>Contraseña: ".$randomString."</h3>
                                                ";
                                            $mail->CharSet = 'UTF-8';
                
                                            $mail->send();
                                            $res['status'] = 1;
                                            $res['message'] = 'Correo Envido';
                                        } 
                                        catch (Exception $e) {
                                            $res['status'] = 0;
                                            $res['message'] = "Correo no envido Error: {$mail->ErrorInfo}";
                                        }
                                    } else {
                                        $res['status'] = 0;
                                        $res['message'] = $resultado[1];
                                    }
                                } else {
                                    $res['status'] = 0;
                                    $res['message'] = "Este correo no existe";
                                }
                            } else {
                                $res['status'] = 0;
                                $res['message'] = $resultado[1];
                            }
                        }
                    }
                break;
                case 'changePassword':
                #verificar que los Post fueron mandados correctamente
                    if (isset($_POST['nombre']) && isset($_POST['apellido']) 
                    && isset($_POST['contra']) && isset($_POST['repetir'])) {
                        #Validar similitud entre contraseña y confirmar contraseña
                        if ($val->contra($_POST['contra'], $_POST['repetir'])) {
                            $n = strtolower($_POST['nombre']);
                            $a = strtolower($_POST['apellido']);
                            $r = strtolower($_POST['contra']);
                            #Validar que nombre y/o apellido este contenido en la contraseña
                            if ($val->numeros(strrpos($r, $n)) == false && $val->numeros(strrpos($r, $a)) == false) {
                                $resultado = $crud->contraVerify(array(
                                    $_SESSION['id_empleado']
                                ));
                                if ($resultado[0] == 1) {
                                    if (count($resultado[1]) > 0) {
                                        if ( password_verify($_POST['contra'], $resultado[1][0]['contra']) ) {
                                            $res['status'] = 0;
                                            $res['message'] = 'La contraseña es igual a la anterior';
                                        }
                                        else {#Ingresar primer usuario
                                            $resultado = $crud->changePasswordId(array(
                                                password_hash($_POST['contra'], PASSWORD_DEFAULT),
                                                $_SESSION['id_empleado']
                                            ));
                                            #validar si todo esta correcto
                                            if ($resultado[0] == 1) {
                                                $res['dataset'] = $resultado[1];
                                                $res['status'] = 1;
                                            } elseif ($resultado[0] == 0) {
                                                $res['message'] = $resultado[1];
                                                $res['status'] = 0;
                                            }
                                        }
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Su usuario dejo de existir';
                                    }
                                }
                                else if ($resultado[0] == 0) {
                                    $res['status'] = 0;
                                    $res['message'] = $resultado[1];
                                }
                            } else {
                                $res['message'] = 'Se encontro similitudes en la contraseña y el nombre';
                                $res['status'] = 0;
                            }
                        } else {
                            $res['message'] = 'Contraseñas no validas';
                            $res['status'] = 0;
                        }
                    } 
                    else {
                        $res['message'] = 'Valores Vacíos';
                        $res['status'] = 0;
                    }
                break;
                case 'cerrarSesion':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->cerrarSesion(array(
                            $_SESSION['id_empleado']
                        ));
                        #validar si todo esta correcto
                        if ($resultado[0] == 1) {
                            $_SESSION['id_empleado'] = null;
                            $res['status'] = 1;
                        } elseif ($resultado[0] == 0) {
                            $res['message'] = 'Error: '.$resultado[1];
                            $res['status'] = 0;
                        }
                    }
                    else {
                        $res['message'] = 'No hay sesion';
                        $res['status'] = 0;
                    }
                break;
                default:
                    $res['message'] = 'Error 0002';
                    $res['status'] = 0;
                break;

            }
        break;
        case 'public':

        break;
        default:
            $res['message'] = 'Error 0001';
            $res['status'] = 0;
        break;
    }
    header('Content-type: application/json');
    echo json_encode($res);
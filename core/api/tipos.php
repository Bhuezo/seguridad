<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    include('../models/CRUD_tipos.php');
    include('../helpers/validaciones.php');
    ini_set('date.timezone', 'America/El_Salvador');
    session_start();
    $crud =  new Tipos();
    $val = new Validacion();

    #Variable que verificara el privilegio osea si es para dashboard o para public la consulta
    $privilege = '';
    #variable que contiene la accion que se solicita
    $action = '';

    #Verificar que si se a mandado el privilegio
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }
    #Varfiicar que se a mandado la acción a realizar
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    $res = null;
    #verificar el privilegio
    switch ($privilege) {
        case 'private':
            #Acciones que se pueden realizar ya iniciando sesion
            switch ($action) {
                #Apis para con sesion
                case 'createE':
                    if (isset($_POST['tipo']) && isset($_POST['atributos'])) {
                        if($val->letras($_POST['tipo'])){
                            if($val->numeros($_POST['atributos'])){
                                $resultado = $crud->createE(array(
                                    $_POST['tipo'],
                                    $_POST['atributos']
                                ));
                                if ($resultado[0] == 1) {
                                    $res['status'] = 1;
                                    $res['dataset'] = $resultado[1];
                                }
                                else if ($resultado[0] == 0) {
                                    $res['status'] = 0;
                                    $res['message'] = $resultado[1];
                                }
                            }
                            else {
    
                            }
                        }
                        else {

                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = "Datos vacios";
                    }
                break;
                case 'readE':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->readE();
                        if ($resultado[0] == 1) {
                            if (count($resultado[1]) > 0) {
                                $res['status'] = 1;
                                $res['dataset'] = $resultado[1];
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No hay usuarios';
                            }
                        }
                        else if ($resultado[0] == 0) {
                            $res['status'] = 0;
                            $res['message'] = $resultado[1];
                        }
                    }
                    else {
                        $res['status'] = 0;
                    }
                break;
                case 'readEmpleados':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->readTiposE();
                        if ($resultado[0] == 1) {
                            if (count($resultado[1]) > 0) {
                                $res['status'] = 1;
                                $res['dataset'] = $resultado[1];
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No hay usuarios';
                            }
                        }
                        else if ($resultado[0] == 0) {
                            $res['status'] = 0;
                            $res['message'] = $resultado[1];
                        }
                    }
                    else {
                        $res['status'] = 0;
                    }
                break;
                case 'updateE':
                    if (isset($_POST['tipo']) && isset($_POST['atributos']) && !empty($_POST['id'])) {
                        if($val->letras($_POST['tipo'])){
                            if($val->numeros($_POST['atributos'])){
                                $resultado = $crud->updateE(array(
                                    $_POST['tipo'],
                                    $_POST['atributos'],
                                    $_POST['id']
                                ));
                                if ($resultado[0] == 1) {
                                    $res['status'] = 1;
                                    $res['dataset'] = $resultado[1];
                                }
                                else if ($resultado[0] == 0) {
                                    $res['status'] = 0;
                                    $res['message'] = $resultado[1];
                                }
                            }
                            else {

                            }
                        }
                        else {

                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = "Datos vacios";
                    }
                break;
                #Apis para con sesion
                case 'createC':
                    if (isset($_POST['tipo']) && isset($_POST['atributos'])) {
                        if($val->letras($_POST['tipo'])){
                            if($val->numeros($_POST['atributos'])){
                                $resultado = $crud->createC(array(
                                    $_POST['tipo'],
                                    $_POST['atributos']
                                ));
                                if ($resultado[0] == 1) {
                                    $res['status'] = 1;
                                    $res['dataset'] = $resultado[1];
                                }
                                else if ($resultado[0] == 0) {
                                    $res['status'] = 0;
                                    $res['message'] = $resultado[1];
                                }
                            }
                            else {
    
                            }
                        }
                        else {

                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = "Datos vacios";
                    }
                break;
                case 'readC':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->readC();
                        if ($resultado[0] == 1) {
                            if (count($resultado[1]) > 0) {
                                $res['status'] = 1;
                                $res['dataset'] = $resultado[1];
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No hay usuarios';
                            }
                        }
                        else if ($resultado[0] == 0) {
                            $res['status'] = 0;
                            $res['message'] = $resultado[1];
                        }
                    }
                    else {
                        $res['status'] = 0;
                    }
                break;
                case 'readClientes':
                    if (isset($_SESSION['id_empleado'])) {
                        $resultado = $crud->readTiposC();
                        if ($resultado[0] == 1) {
                            if (count($resultado[1]) > 0) {
                                $res['status'] = 1;
                                $res['dataset'] = $resultado[1];
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No hay usuarios';
                            }
                        }
                        else if ($resultado[0] == 0) {
                            $res['status'] = 0;
                            $res['message'] = $resultado[1];
                        }
                    }
                    else {
                        $res['status'] = 0;
                    }
                break;
                case 'updateC':
                    if (isset($_POST['tipo']) && isset($_POST['atributos']) && !empty($_POST['id'])) {
                        if($val->letras($_POST['tipo'])){
                            if($val->numeros($_POST['atributos'])){
                                $resultado = $crud->updateC(array(
                                    $_POST['tipo'],
                                    $_POST['atributos'],
                                    $_POST['id']
                                ));
                                if ($resultado[0] == 1) {
                                    $res['status'] = 1;
                                    $res['dataset'] = $resultado[1];
                                }
                                else if ($resultado[0] == 0) {
                                    $res['status'] = 0;
                                    $res['message'] = $resultado[1];
                                }
                            }
                            else {

                            }
                        }
                        else {

                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = "Datos vacios";
                    }
                break;
                default:
                    $res['message'] = 'Error 0002';
                    $res['status'] = 0;
                break;

            }
        break;
        case 'public':

        break;
        default:
            $res['message'] = 'Error 0001';
            $res['status'] = 0;
        break;
    }
    header('Content-type: application/json');
    echo json_encode($res);
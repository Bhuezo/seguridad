<?php
    class Empleados
    {
        #Modelo para validar primer usuario
        public function valPrimero(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "Select * from usuarios";
            return $app->crud('SELECT',$sql,null,null);
        }
        public function readEmpleado(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT id_usuario as 'id', nombre, apellido, correo, alias, 
            tipo, intentos, fecha_nacimiento as 'fecha', usuarios.id_tipo
            from usuarios INNER JOIN tipo_empleado using(id_tipo)";
            return $app->crud('SELECT',$sql,null,null);
        }
        public function primerUser($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "INSERT INTO usuarios VALUES (null,?,?,?,?,?,now(),?,(select id_tipo FROM tipo_empleado ORDER BY id_tipo DESC limit 0,1),0,now(),0)";
            return $app->crud6('INSERT',$sql,"ssssss",$dato[0],$dato[1],$dato[2],$dato[3],password_hash($dato[4], PASSWORD_DEFAULT),$dato[5]);
            
        }
        public function updatePersonal($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios SET alias = ?, nombre = ?, apellido = ?, correo = ?, fecha_nacimiento = ? WHERE id_usuario = ?";
            return $app->crud6('UPDATE',$sql,"sssssi",$dato[0],$dato[1],$dato[2],$dato[3],$dato[4],$dato[5]);
            
        }
        public function primerPrivilege(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "INSERT INTO tipo_empleado VALUES (null,'admin','1111')";
            return $app->crud('INSERT',$sql,null,null);
        }
        public function login($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "Select id_usuario,contra,intentos,estado,fecha_estado, atributos 
            from usuarios inner join tipo_empleado using(id_tipo)
            where alias = ? AND intentos < 3";
            return $app->crud('SELECT',$sql,"s",$dato[0]);

            
        }
        public function readPersonal($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT alias,nombre,apellido,correo,fecha_nacimiento as 'fecha',fecha_contra,intentos,atributos,tipo FROM tipo_empleado inner join usuarios using(id_tipo) where id_usuario = ?";
            return $app->crud('SELECT',$sql,"i",$dato[0]);

            
        }
        public function contraVerify($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT contra FROM usuarios where id_usuario = ?";
            return $app->crud('SELECT',$sql,"i",$dato[0]);

            
        }
        public function valCorreo($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "Select correo from usuarios where correo = ?";
            return $app->crud('SELECT',$sql,"s",$dato[0]);

            
        }
        public function ingresar($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios set intentos = 0, fecha_estado = now(), estado = 1 where alias = ?";
            return $app->crud('UPDATE',$sql,"s",$dato[0]);

            
        }
        public function intentos($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios set intentos = ? where alias = ?";
            return $app->crud2('UPDATE',$sql,"is",$dato[0],$dato[1]);

            
        }
        public function changePassword($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios set contra = ? where correo = ?";
            return $app->crud2('UPDATE',$sql,"ss",$dato[0],$dato[1]);

            
        }
        public function changePasswordId($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios set contra = ?, fecha_contra = now() where id_usuario = ?";
            return $app->crud2('UPDATE',$sql,"ss",$dato[0],$dato[1]);

            
        }
        public function bloquear($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios set id_tipo = (select id_tipo from tipo_empleado where atributos = '0000' limit 0,1) where alias = ?";
            return $app->crud('UPDATE',$sql,"s",$dato[0]);

            
        }
        public function cerrarSesion($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios set estado = 0, fecha_estado = '0000-00-00' where id_usuario = ?";
            return $app->crud('UPDATE',$sql,"i",$dato[0]);

            
        }
        #CRUD

        public function create($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "INSERT INTO usuarios VALUES (null,?,?,?,?,?,now(),?,?,0,now(),?)";
            return $app->crud8('INSERT',$sql,"sssssssi",$dato[0],$dato[1],$dato[2],$dato[3],$dato[4],$dato[5],$dato[6],$dato[7]);
            
        }
        public function update($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios SET alias = ?, nombre = ?, apellido = ?, correo = ?, fecha_nacimiento = ?, id_tipo = ? ,intentos = ? , contra = ? WHERE id_usuario = ?";
            return $app->crud9('UPDATE',$sql,"ssssssssi",$dato[0],$dato[1],$dato[2],$dato[3],$dato[4],$dato[5],$dato[6],$dato[7],$dato[8]);
            
        }
        public function updateSP($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE usuarios SET alias = ?, nombre = ?, apellido = ?, correo = ?, fecha_nacimiento = ?, id_tipo = ? ,intentos = ? WHERE id_usuario = ?";
            return $app->crud8('UPDATE',$sql,"sssssssi",$dato[0],$dato[1],$dato[2],$dato[3],$dato[4],$dato[5],$dato[6],$dato[7]);
            
        }
    }
?>
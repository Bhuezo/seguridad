<?php
    class Tipos
    {
        #Modelo para validar primer usuario
        public function readTiposE(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT id_tipo, tipo from tipo_empleado ORDER BY atributos DESC";
            return $app->crud('SELECT',$sql,null,null);
        }
        public function readE(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT id_tipo as 'id', tipo, atributos from tipo_empleado ORDER BY atributos DESC";
            return $app->crud('SELECT',$sql,null,null);
        }
        public function createE($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "INSERT INTO tipo_empleado VALUES (null,?,?)";
            return $app->crud2('INSERT',$sql,"ss",$dato[0],$dato[1]);
            
        }
        public function updateE($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE tipo_empleado SET tipo = ?, atributos = ? WHERE id_tipo = ?";
            return $app->crud3('UPDATE',$sql,"ssi",$dato[0],$dato[1],$dato[2]);
            
        }
        public function readTiposC(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT * from tipo_cliente ORDER BY atributos DESC";
            return $app->crud('SELECT',$sql,null,null);
        }
        public function readC(){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "SELECT id_tipo as 'id', tipo, atributos from tipo_cliente ORDER BY atributos DESC";
            return $app->crud('SELECT',$sql,null,null);
        }
        public function createC($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "INSERT INTO tipo_cliente VALUES (null,?,?)";
            return $app->crud2('INSERT',$sql,"ss",$dato[0],$dato[1]);
            
        }
        public function updateC($dato){
            include_once('../helpers/conexion.php');
            $app = new Conexion();

            $sql = "UPDATE tipo_cliente SET tipo = ?, atributos = ? WHERE id_tipo = ?";
            return $app->crud3('UPDATE',$sql,"ssi",$dato[0],$dato[1],$dato[2]);
            
        }
    }
?>
//Convertidor de $_POST, los datos requeridos
serialize = function(obj, prefix) {
    var str = [],
      p;
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        str.push((v !== null && typeof v === "object") ?
          serialize(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}
const reg1 = new Vue({
    el: '#reg1',
    data:{
        nombre: 'hola',
        //Errores entre otras cosas
        error: '',
        alert: false,
        color: false,
        //Formulario
        fm: {
            id: '',
            alias: '',
            nombre: '',
            apellido: '',
            correo: '',
            contra: '',
            repetir: '',
            fecha: '',
        },
        Abtn: true,
        valFecha: '0000-00-00',
        btn: true,
    },
    mounted() {
        //validar fecha 
        let fecha = new Date()
        fecha.setFullYear(fecha.getFullYear() - 18 )
        let dia = fecha.getDate().toString();
        dia = dia.length > 1 ? fecha.getDate() : '0'+fecha.getDate()

        let mes = fecha.getMonth().toString();
        mes = mes.length > 1 ? fecha.getMonth() : '0'+fecha.getMonth()

        let ano = fecha.getFullYear()
        this.valFecha = ano+'-'+mes+'-'+dia
        this.fm.fecha = this.valFecha
    },
    computed: {
        Vnombre: function (){
            if(this.fm.nombre.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.nombre) && this.fm.nombre.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vapellido: function (){
            if(this.fm.apellido.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.apellido) && this.fm.apellido.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vcorreo: function (){
            if(this.fm.correo.length > 4 && this.fm.correo.trim() != '' && /^[a-zA-Z0-9._]+@[a-zA-Z0-9]+.([a-zA-Z])*$/.test(this.fm.correo)){
                return true;
            }
            else{
                return false;
            }
        },
        Vcontra: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.contra.trim() != '' && regex.test(this.fm.contra) && this.fm.contra.toLowerCase().indexOf(this.fm.nombre.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.apellido.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.alias.toLowerCase())){
                return true;
            }
            else{
                return false;
            }
        },
        Vrepetir: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.repetir == this.fm.contra && this.fm.repetir.trim() != '' && regex.test(this.fm.repetir) && this.fm.contra.toLowerCase().indexOf(this.fm.nombre.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.apellido.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.alias.toLowerCase())){
                return true;
            }
            else{
                return false;
            }
        },
        Vfecha: function (){
            let fecha 
            if(this.fm.fecha.length > 8){
                return true;
            }
            else{
                return false;
            }
        },
        Valias: function(){
            if (this.fm.alias.trim() != '' && /^([a-zA-Z0-9])*$/.test(this.fm.alias) && this.fm.alias.length >= 4) {
                return true;
            } else {
                return false;
            }
        }
    },
    methods: {
        setCliente(){
            this.Abtn = false
            let data = this.fm
            let d = this
            //Esto ejecuta el captcha para valdiar si eres una maquina
            grecaptcha.execute('6LcbrrQUAAAAALtcPkA92VqK2bVa98loPms2s2BI', {action: 'homepage'})
            .then(function(token) {
                //console.log(token)
                //Validamos el token para ver que todo esta bien
                axios.post('../../core/api/reCaptcha.php',serialize({
                    response: token,
                }))
                .then(res1=>{
                    //console.log(res)
                    if(res1.data.status == 1){
                        //Luego de validar de que no eres un robot ingresa el usuario
                        let link = '../../core/api/clientes.php?privilege=public&action=registrarN'
                        axios.post(link, serialize(data))
                        .then(res=>{
                            console.log(res)
                            if (res.data.status == 1) {
                                d.alert = true
                                d.color = true
                                d.error = res.data.dataset
                                d.Abtn = true
                                d.fm = {
                                    id: '',
                                    alias: '',
                                    nombre: '',
                                    apellido: '',
                                    correo: '',
                                    contra: '',
                                    repetir: '',
                                    fecha: this.valFecha,
                                }
                            }
                            else if (res.data.status == 0) {
                                d.alert = true
                                d.color = false
                                d.error = res.data.message
                                d.Abtn = true
                            }
                        })
                    }
                    else if(res1.data.status == 0){
                        alert("Ahhh perrro");
                        
                    }
                })
            });

        },
    },
})

const reg2 = new Vue({
    el: '#reg2',
    data:{
        nombre: 'hola',
        //Errores entre otras cosas
        error: '',
        alert: false,
        color: false,
        //Formulario
        fm: {
            id: '',
            alias: '',
            nombre: '',
            apellido: '',
            correo: '',
            contra: '',
            repetir: '',
            fecha: '',
        },
        Abtn: true,
        valFecha: '0000-00-00',
        btn: true,
    },
    mounted() {
        //validar fecha 
        let fecha = new Date()
        fecha.setFullYear(fecha.getFullYear() - 18 )
        let dia = fecha.getDate().toString();
        dia = dia.length > 1 ? fecha.getDate() : '0'+fecha.getDate()

        let mes = fecha.getMonth().toString();
        mes = mes.length > 1 ? fecha.getMonth() : '0'+fecha.getMonth()

        let ano = fecha.getFullYear()
        this.valFecha = ano+'-'+mes+'-'+dia
        this.fm.fecha = this.valFecha
    },
    computed: {
        Vnombre: function (){
            if(this.fm.nombre.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.nombre) && this.fm.nombre.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vapellido: function (){
            if(this.fm.apellido.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.apellido) && this.fm.apellido.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vcorreo: function (){
            if(this.fm.correo.length > 4 && this.fm.correo.trim() != '' && /^[a-zA-Z0-9._]+@[a-zA-Z0-9]+.([a-zA-Z])*$/.test(this.fm.correo)){
                return true;
            }
            else{
                return false;
            }
        },
        Vcontra: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.contra.trim() != '' && regex.test(this.fm.contra) && this.fm.contra.toLowerCase().indexOf(this.fm.nombre.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.apellido.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.alias.toLowerCase())){
                return true;
            }
            else{
                return false;
            }
        },
        Vrepetir: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.repetir == this.fm.contra && this.fm.repetir.trim() != '' && regex.test(this.fm.repetir) && this.fm.contra.toLowerCase().indexOf(this.fm.nombre.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.apellido.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.alias.toLowerCase())){
                return true;
            }
            else{
                return false;
            }
        },
        Vfecha: function (){
            let fecha 
            if(this.fm.fecha.length > 8){
                return true;
            }
            else{
                return false;
            }
        },
        Valias: function(){
            if (this.fm.alias.trim() != '' && /^([a-zA-Z0-9])*$/.test(this.fm.alias) && this.fm.alias.length >= 4) {
                return true;
            } else {
                return false;
            }
        }
    },
    methods: {
        setCliente(){
            this.Abtn = false
            let data = this.fm
            let d = this
            //Esto ejecuta el captcha para valdiar si eres una maquina
            grecaptcha.execute('6LcbrrQUAAAAALtcPkA92VqK2bVa98loPms2s2BI', {action: 'homepage'})
            .then(function(token) {
                //console.log(token)
                //Validamos el token para ver que todo esta bien
                axios.post('../../core/api/reCaptcha.php',serialize({
                    response: token,
                }))
                .then(res1=>{
                    //console.log(res)
                    if(res1.data.status == 1){
                        //Luego de validar de que no eres un robot ingresa el usuario
                        let link = '../../core/api/clientes.php?privilege=public&action=registrarV'
                        axios.post(link, serialize(data))
                        .then(res=>{
                            console.log(res)
                            if (res.data.status == 1) {
                                d.alert = true
                                d.color = true
                                d.error = res.data.dataset
                                d.Abtn = true
                                d.fm = {
                                    id: '',
                                    alias: '',
                                    nombre: '',
                                    apellido: '',
                                    correo: '',
                                    contra: '',
                                    repetir: '',
                                    fecha: this.valFecha,
                                }
                            }
                            else if (res.data.status == 0) {
                                d.alert = true
                                d.color = false
                                d.error = res.data.message
                                d.Abtn = true
                            }
                        })
                    }
                    else if(res1.data.status == 0){
                        alert("Ahhh perrro");
                        
                    }
                })
            });

        }
    },
})
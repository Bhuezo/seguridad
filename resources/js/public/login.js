//Convertidor de $_POST, los datos requeridos
serialize = function(obj, prefix) {
    var str = [],
      p;
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        str.push((v !== null && typeof v === "object") ?
          serialize(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}
const app = new Vue({
    el: '#app',
    data:{
        //Errores entre otras cosas
        error: '',
        alert: false,
        alert2: false,
        color: false,
        Abtn: true,
        Bbtn: true,
        //Formulario
        fm: {
            alias: '',
            correo: '',
            contra: '',
        },
        //Olvide la contraseña
        olvide: false,
    },
    mounted() {
        this.valSesion()
    },
    computed: {
        Valias: function (){
            if(this.fm.alias.length > 3 && this.fm.alias.trim() != '' ){
                return true;
            }
            else{
                return false;
            }
        },
        Vcorreo: function (){
            if(this.fm.correo.length > 4 && this.fm.correo.trim() != '' && /^[a-zA-Z0-9._]+@[a-zA-Z0-9]+.([a-zA-Z])*$/.test(this.fm.correo)){
                return true;
            }
            else{
                return false;
            }
        },
        Vcontra: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.contra.trim() != '' && regex.test(this.fm.contra) ){
                return true;
            }
            else{
                return false;
            }
        },
    },
    methods: {
        valSesion(){
            let link = '../../core/api/clientes.php?privilege=public&action=sesion'
            axios.get(link)
            .then(res=>{
                //console.log(res)
                if (res.data.status == 1) {
                    location.href = 'perfil.html'
                }
                else if (res.data.status == 0) {
                }
            })
        },
        login(){
            this.Abtn = false
            this.alert2 = false
            this.olvide = false
            let data = this.fm
            let d = this
            //Esto ejecuta el captcha para valdiar si eres una maquina
            grecaptcha.execute('6LcbrrQUAAAAALtcPkA92VqK2bVa98loPms2s2BI', {action: 'homepage'})
            .then(function(token) {
                //console.log(token)
                //Validamos el token para ver que todo esta bien
                axios.post('../../core/api/reCaptcha.php',serialize({
                    response: token,
                }))
                .then(res1=>{
                    //console.log(res)
                    if(res1.data.status == 1){
                        //Luego de validar de que no eres un robot ingresa el usuario
                        let link = '../../core/api/clientes.php?privilege=public&action=login'
                        axios.post(link, serialize(data))
                        .then(res=>{
                            console.log(res)
                            if (res.data.status == 1) {
                                location.reload()
                            }
                            else if (res.data.status == 0) {
                                d.alert = true
                                d.color = false
                                d.error = res.data.message
                                d.Abtn = true
                            }
                        })
                    }
                    else if(res1.data.status == 0){
                        alert("Ahhh perrro");
                        
                    }
                })
            });

        },
        olvidePassword(){
            this.alert = false
            this.Bbtn = false
            let data = this.fm
            let d = this
            //Esto ejecuta el captcha para valdiar si eres una maquina
            grecaptcha.execute('6LcbrrQUAAAAALtcPkA92VqK2bVa98loPms2s2BI', {action: 'homepage'})
            .then(function(token) {
                //console.log(token)
                //Validamos el token para ver que todo esta bien
                axios.post('../../core/api/reCaptcha.php',serialize({
                    response: token,
                }))
                .then(res1=>{
                    //console.log(res)
                    if(res1.data.status == 1){
                        //Luego de validar de que no eres un robot ingresa el usuario
                        let link = '../../core/api/clientes.php?privilege=public&action=requiredPassword'
                        axios.post(link, serialize(data))
                        .then(res=>{
                            console.log(res)
                            if (res.data.status == 1) {
                                d.alert2 = true
                                d.color = true
                                d.error = res.data.message
                                d.Bbtn = true
                            }
                            else if (res.data.status == 0) {
                                d.alert2 = true
                                d.color = false
                                d.error = res.data.message
                                d.Bbtn = true
                            }
                        })
                    }
                    else if(res1.data.status == 0){
                        alert("Ahhh perrro");
                        
                    }
                })
            });
        }
    },
})
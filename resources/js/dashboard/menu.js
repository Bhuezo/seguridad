//Convertidor de $_POST, los datos requeridos
serialize = function (obj, prefix) {
    var str = [],
        p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p,
                v = obj[p];
            str.push((v !== null && typeof v === "object") ?
                serialize(v, k) :
                encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
}
var idleTime = 0;
$(document).ready(function () {
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 7) {
        let link = '../../core/api/empleados.php?privilege=private&action=cerrarSesion'
            axios.post(link)
                .then(res => {
                    console.log(res)
                    if (res.data.status == 1) {
                        location.reload()
                    } else if (res.data.status == 0) {
                        location.href = 'index.html';
                    }
                })
    }
}
const menu = new Vue({
    el: '#menu',
    data: {
        nombre: 'hola',
        //Errores entre otras cosas
        error: '',
        alert: false,
        color: false,
        //Formulario
        fm: {
            id: '',
            nombre: '',
            alias: '',
            apellido: '',
            correo: '',
            contra: '',
            repetir: '',
            fecha: '',
            tipo: '',
        },
        menu: [
            
        ],
        Abtn: true,
        valFecha: '0000-00-00',
        btn: true,
    },
    mounted() {
        MicroModal.init();
        this.valSesion();
    },
    computed: {
        Vnombre: function () {
            if (this.fm.nombre.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.nombre) && this.fm.nombre.length > 2) {
                return true;
            } else {
                return false;
            }
        },
        Vapellido: function () {
            if (this.fm.apellido.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.apellido) && this.fm.apellido.length > 2) {
                return true;
            } else {
                return false;
            }
        },
        Vcorreo: function () {
            if (this.fm.correo.length > 4 && this.fm.correo.trim() != '' && /^[a-zA-Z0-9._]+@[a-zA-Z0-9]+.([a-zA-Z])*$/.test(this.fm.correo)) {
                return true;
            } else {
                return false;
            }
        },
        Vcontra: function () {
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if (this.fm.contra.trim() != '' && regex.test(this.fm.contra) && this.fm.contra.toLowerCase().indexOf(this.fm.nombre.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.apellido.toLowerCase())) {
                return true;
            } else {
                return false;
            }
        },
        Vrepetir: function () {
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if (this.fm.repetir == this.fm.contra && this.fm.repetir.trim() != '' && regex.test(this.fm.repetir) && this.fm.contra.toLowerCase().indexOf(this.fm.nombre.toLowerCase()) && this.fm.contra.toLowerCase().indexOf(this.fm.apellido.toLowerCase())) {
                return true;
            } else {
                return false;
            }
        },
        Vfecha: function () {
            let fecha
            if (this.fm.fecha.length > 8) {
                return true;
            } else {
                return false;
            }
        },
        Valias: function () {
            if (this.fm.alias.trim() != '' && /^([a-zA-Z0-9])*$/.test(this.fm.alias) && this.fm.alias.length >= 4) {
                return true;
            } else {
                return false;
            }
        },
    },
    methods: {
        setCliente(e) {
            this.Abtn = false
            let data = this.fm
            let d = this
            //Luego de validar de que no eres un robot ingresa el usuario
            let link = '../../core/api/empleados.php?privilege=private&action=updatePersonal'
            axios.post(link, serialize(data))
                .then(res => {
                    console.log(res)
                    if (res.data.status == 1) {
                        d.alert = true
                        d.color = true
                        d.error = res.data.dataset
                        d.Abtn = true
                        //location.reload()
                    } else if (res.data.status == 0) {
                        d.alert = true
                        d.color = false
                        d.error = res.data.message
                        d.Abtn = true
                    }
                })

        },
        setPassword(e) {
            this.Abtn = false
            let data = this.fm
            let d = this
            //Luego de validar de que no eres un robot ingresa el usuario
            let link = '../../core/api/empleados.php?privilege=private&action=changePassword'
            axios.post(link, serialize(data))
                .then(res => {
                    console.log(res)
                    if (res.data.status == 1) {
                        d.alert = true
                        d.color = true
                        d.error = res.data.dataset
                        d.Abtn = true
                        //location.reload()
                    } else if (res.data.status == 0) {
                        d.alert = true
                        d.color = false
                        d.error = res.data.message
                        d.Abtn = true
                    }
                })

        },
        valSesion(){
            let d = this.fm
            //Luego de validar de que no eres un robot ingresa el usuario
            let link = '../../core/api/empleados.php?privilege=private&action=readPersonal'
            axios.post(link)
                .then(res => {
                    console.log(res)
                    if (res.data.status == 1) {
                        let datos = res.data.dataset
                        d.alias = datos[0].alias
                        d.nombre = datos[0].nombre
                        d.apellido = datos[0].apellido
                        d.fecha = datos[0].fecha
                        d.tipo = datos[0].tipo
                        d.correo = datos[0].correo
                        let fecha = new Date (datos[0].fecha_contra)
                        fecha.setDate(fecha.getDate() + 90)
                        let hoy = new Date()
                        if (fecha <= hoy) {
                            alert('Se recomienda cambiar contraseña');
                            MicroModal.show('modal-2')
                        }
                        else {
                            if ( datos[0].atributos.charAt(0) == 1) {
                                this.menu.push({
                                    nombre: 'Empleados', link: 'empleados.html'
                                })
                            }
                            if ( datos[0].atributos.charAt(1) == 1) {
                                this.menu.push({
                                    nombre: 'Clientes', link: 'clientes.html'
                                })
                            }
                            if ( datos[0].atributos.charAt(2) == 1) {
                                this.menu.push({
                                    nombre: 'Productos', link: 'productos.html'
                                })
                            }
                            if ( datos[0].atributos.charAt(3) == 1) {
                                this.menu.push({
                                    nombre: 'Facturas', link: 'facturas.html'
                                })
                            }
                        }

                    } else if (res.data.status == 0) {
                        location.href = 'index.html';
                    }
                })
        },
        cerrarSesion(){
            //Luego de validar de que no eres un robot ingresa el usuario
            let link = '../../core/api/empleados.php?privilege=private&action=cerrarSesion'
            axios.post(link)
                .then(res => {
                    console.log(res)
                    if (res.data.status == 1) {
                        location.reload()
                    } else if (res.data.status == 0) {
                        location.href = 'index.html';
                    }
                })
        },
        iniciar(e) {
            MicroModal.show(e)
        }
    },
})
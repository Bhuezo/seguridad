//Convertidor de $_POST, los datos requeridos
serialize = function(obj, prefix) {
    var str = [],
      p;
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        str.push((v !== null && typeof v === "object") ?
          serialize(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}
const app = new Vue({
    el: '#app',
    data:{
        nombre: 'hola',
        //Errores entre otras cosas
        error: '',
        alert: false,
        color: false,
        //Formulario
        fm: {
            id: '',
            alias: '',
            nombre: '',
            apellido: '',
            correo: '',
            contra: '',
            repetir: '',
            fecha: '',
            intentos: '',
            tipo: 'Elegir',
            id_tipo: 0,
        },
        valFecha: '0000-00-00',
        btn: true,
        empleados: [],
        tipos: [],
    },
    mounted() {
        let fecha = new Date()
        fecha.setFullYear(fecha.getFullYear() - 18 )
        let dia = fecha.getDate().toString();
        dia = dia.length > 1 ? fecha.getDate() : '0'+fecha.getDate()

        let mes = fecha.getMonth().toString();
        mes = mes.length > 1 ? fecha.getMonth() : '0'+fecha.getMonth()

        let ano = fecha.getFullYear()
        this.valFecha = ano+'-'+mes+'-'+dia
        this.fm.fecha = this.valFecha
        this.getEmpleados()
        this.getTipos()
    },
    computed: {
        Vnombre: function (){
            if(this.fm.nombre.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.nombre) && this.fm.nombre.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vapellido: function (){
            if(this.fm.apellido.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.apellido) && this.fm.apellido.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vcorreo: function (){
            if(this.fm.correo.length > 4 && this.fm.correo.trim() != '' && /^[a-zA-Z0-9._]+@[a-zA-Z0-9]+.([a-zA-Z])*$/.test(this.fm.correo)){
                return true;
            }
            else{
                return false;
            }
        },
        Vcontra: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.contra.trim() != '' && regex.test(this.fm.contra) && this.fm.contra.indexOf(this.fm.nombre) && this.fm.contra.indexOf(this.fm.apellido)){
                return true;
            }
            else{
                return false;
            }
        },
        Vrepetir: function (){
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            if(this.fm.repetir == this.fm.contra && this.fm.repetir.trim() != '' && regex.test(this.fm.repetir)){
                return true;
            }
            else{
                return false;
            }
        },
        Vfecha: function (){
            let fecha 
            if(this.fm.fecha.length > 8){
                return true;
            }
            else{
                return false;
            }
        },
        Valias: function () {
            if (this.fm.alias.trim() != '' && /^([a-zA-Z0-9])*$/.test(this.fm.alias) && this.fm.alias.length >= 4) {
                return true;
            } else {
                return false;
            }
        },
        Vintentos: function () {
            if (/^([0-9])*$/.test(this.fm.intentos) && this.fm.intentos < 4) {
                return true;
            } else {
                return false;
            }
        },
        Vtipo(){
            let datos = this.tipos.filter(todo =>{
                return  todo.id_tipo == this.fm.id_tipo;
                
            })
            if (datos.length > 0){
                return datos[0].tipo
            }
            else  return 'Elegir'
        }
    },
    methods: {
        getEmpleados(){
            let link = '../../core/api/clientes.php?privilege=private&action=read'
            axios.post(link)
            .then(res=>{
                console.log(res)
                if (res.data.status == 1) {
                    this.empleados = res.data.dataset
                }
                else if (res.data.status == 0) {
                    this.alert = true
                    this.color = false
                    this.error = res.data.message
                }
            })
        },
        getTipos(){
            let link = '../../core/api/tipos.php?privilege=private&action=readClientes'
            axios.post(link)
            .then(res=>{
                console.log(res)
                if (res.data.status == 1) {
                    this.tipos = res.data.dataset
                }
                else if (res.data.status == 0) {
                    this.alert = true
                    this.color = false
                    this.error = res.data.message
                }
            })
        },
        actualizar(e){
            console.log(e)
            this.fm.id = e.id
            this.fm.alias = e.alias
            this.fm.nombre = e.nombre
            this.fm.apellido = e.apellido
            this.fm.correo = e.correo
            this.fm.id_tipo = e.id_tipo
            this.fm.fecha = e.fecha
            this.fm.intentos = e.intentos
            this.fm.contra = ''
            this.fm.repetir = ''
        },
        tipos(el){
            console.log(e)
        },
        setEmpleados(){
            let data = this.fm
            let link = ''
            if (this.btn == true) {
                link = '../../core/api/clientes.php?privilege=private&action=create'
            }
            else {
                link = '../../core/api/clientes.php?privilege=private&action=update'
            }
            axios.post(link, serialize(data))
            .then(res=>{
                console.log(res)
                if (res.data.status == 1) {
                    this.alert = true
                    this.color = true
                    this.error = res.data.dataset
                    this.getEmpleados()
                }
                else if (res.data.status == 0) {
                    this.alert = true
                    this.color = false
                    this.error = res.data.message
                }
            })
        }
    },
})
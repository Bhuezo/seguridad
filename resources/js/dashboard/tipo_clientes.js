//Convertidor de $_POST, los datos requeridos
serialize = function(obj, prefix) {
    var str = [],
      p;
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        str.push((v !== null && typeof v === "object") ?
          serialize(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}
const tipo = new Vue({
    el: '#tipo',
    data:{
        tipo: 'hola',
        //Errores entre otras cosas
        error: '',
        alert: false,
        color: false,
        //Formulario
        fm: {
            atributos: '',
            tipo: '',
            id: '',
        },
        valFecha: '0000-00-00',
        btn: true,
        empleados: [],
        tipos: [],
    },
    mounted() {
        this.getEmpleados()
    },
    computed: {
        Vtipo: function (){
            if(this.fm.tipo.trim() != '' && /^([A-Z,a-z])*$/.test(this.fm.tipo) && this.fm.tipo.length > 2){
                return true;
            }
            else{
                return false;
            }
        },
        Vatributos: function () {
            if (/^([0-9])*$/.test(this.fm.atributos) && this.fm.atributos.toString().length == 2) {
                return true;
            } else {
                return false;
            }
        },
    },
    methods: {
        getEmpleados(){
            let link = '../../core/api/tipos.php?privilege=private&action=readC'
            axios.post(link)
            .then(res=>{
                console.log(res)
                if (res.data.status == 1) {
                    this.empleados = res.data.dataset
                }
                else if (res.data.status == 0) {
                    this.alert = true
                    this.color = false
                    this.error = res.data.message
                }
            })
        },
        actualizar(e){
            console.log(e)
            this.fm.tipo = e.tipo
            this.fm.id = e.id
            this.fm.atributos = e.atributos
        },
        setEmpleados(){
            let data = this.fm
            let link = ''
            if (this.btn == true) {
                link = '../../core/api/tipos.php?privilege=private&action=createC'
            }
            else {
                link = '../../core/api/tipos.php?privilege=private&action=updateC'
            }
            axios.post(link, serialize(data))
            .then(res=>{
                console.log(res)
                if (res.data.status == 1) {
                    this.alert = true
                    this.color = true
                    this.error = res.data.dataset
                    this.getEmpleados()
                }
                else if (res.data.status == 0) {
                    this.alert = true
                    this.color = false
                    this.error = res.data.message
                }
            })
        }
    },
})